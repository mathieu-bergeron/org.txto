# Txto

## A prototype

1. This is a throw away prototype, there is no plan to develop this prototype passed a "proof of concept" stage
1. It is yet unclear the toolchain that should be used for the final version (aiming for LLVM/WASM?)

## Technologies used and why

1. JavaFx: this is the graphical toolkit I'm teaching right now at Collège Montmorency
1. GF: perhaps the most advance multilingual grammar generator?

## Challenges

1. Input
    * text in simple labelled graphical boxes
    * schemas (notation) is generated, but modifiable
    * later: free-from input recognition

1. Execution
    * via a macth-apply loop

1. AI integration
    * AI should be used to generate readable code
    * only part of that code should refer to statistical models, and in a readable way: "word X is a verb according to model Y"
    
1. Low-level implementation integration
    * the high level Txto code acts as an executable spec
    * we can rewrite part of the spec with low-level efficient code
    * (there should be automatic testing that both implementation match on key examples)
    * (on other examples, it is a question of trust, to be decided in a WARMED way)


### NOTE

* WARMED: Write, Argue, Read, Modify, Execute, Debug
    
